<?php

namespace Maatoo\Maatoo\Model;

/**
 * Class StoreConfigManager
 * @package Maatoo\Maatoo\Model
 */
class StoreConfigManager
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var \Magento\Store\Api\StoreRepositoryInterface
     */
    private $storeRepository;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;


    /**
     * StoreConfigManager constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Api\StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Api\StoreRepositoryInterface $storeRepository
    )
    {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->storeRepository = $storeRepository;
    }

    /**
     * @return array
     */
    public function getStores()
    {
        $stores = [];
        foreach ($this->storeManager->getStores(false) as $store) {
            $websiteAllowed = $this->scopeConfig->getValue(
                \Maatoo\Maatoo\Model\Config\Config::MAATAA_WEBSITE_ACTIVE,
                \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
                $store->getWebsiteId()
            );
            if($websiteAllowed) {
                $storeAllowed = $this->scopeConfig->getValue(
                    \Maatoo\Maatoo\Model\Config\Config::MAATOO_ALLOWED_STORE,
                    \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
                    $store->getWebsiteId()
                );
                if($storeAllowed) {
                    $storesAllowed = explode(',' , $storeAllowed);
                    if (in_array($store->getId(),$storesAllowed)) {
                        $stores[] = $store;
                    }
                }
            }
        }
        return $stores;
    }

    public function getStoreIds()
    {
        $stores = [];
        foreach($this->getStores() as $store)
        {
            $stores[] = $store->getId();
        }
        return $stores;
    }

    public function getStoreShortName(\Magento\Store\Api\Data\StoreInterface $store)
    {
        return preg_replace('#\W+#', '', trim(strtolower($store->getName())));
    }

    public function getTags($store)
    {
        return $this->getStoreShortName($store) . '-pending';
    }

}
